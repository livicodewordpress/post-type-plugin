 <?php

 /**

 * @link              http://example.com
 * @since             1.0.0
 * @package           Remove_welcome_dashboard
 *
 * @wordpress-plugin
 * Plugin Name:       Create a new Post Type
 * Plugin URI:        http://owoyale.com/
 * Description:       This is a wordpress plugin that creates a new post type
 * Version:           1.0.0
 * Author:            Femi Yale
 * Author URI:        http://owoyale.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Create New Post Type
 * Domain Path:       /languages
 */

 //Exit if  accessed directly
if(!defined('ABSPATH')){
	exit;
}

function femiowo_nominees_post_type(){
	$singular = 'Nominee';
	$plural = 'Nominees';


							$labels = array(
										'name' => $plural,
										'singular_name' => $singular,
										'add_new' => _x('Add New', $singular),
										'add_new_item' => __('Add New '.$singular ),
										'edit_item' => __('Edit ' .$singular),
										'new_item' => __('New '.$singular),


								);

							$args = array(
								'public'=>true, 
								'labels'=>$labels,
								'description' => 'All nominees can be enteres here',
								'show_in_admin_bar' => true,
								'menu_position' => 4,
								'show_in_menu' => true,
								'menu_icon' => 'dashicons-universal-access',
								'can_export' => true,
								'supports' => array('thumbnail', 'excerpt', 'custom_fields', 'editor'),

								);


							register_post_type( 'nominees', $args );
}
add_action('init','femiowo_nominees_post_type');